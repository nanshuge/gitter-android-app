package im.gitter.gitter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class BadgeView extends TextView {

    private Drawable mentionBadge;
    private Drawable unreadBadge;
    private Drawable activityBadge;

    public BadgeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mentionBadge = ContextCompat.getDrawable(context, R.drawable.mention_badge);
        unreadBadge = ContextCompat.getDrawable(context, R.drawable.unread_badge);
        activityBadge = ContextCompat.getDrawable(context, R.drawable.activity_badge);
    }

    public void setBadge(int unreadCount, int mentionCount) {
        setBadge(unreadCount, mentionCount, false);
    }

    public void setBadge(int unreadCount, int mentionCount, boolean showActivityIndicator) {
        if (unreadCount > 0 || mentionCount > 0 || showActivityIndicator) {
            setVisibility(View.VISIBLE);
            if (mentionCount > 0) {
                setText("@");
                setBackground(mentionBadge);
            } else if (unreadCount > 0) {
                setText(unreadCount > 99 ? "99+" : String.valueOf(unreadCount));
                setBackground(unreadBadge);
            } else {
                setText("");
                setBackground(activityBadge);
            }
        } else {
            setVisibility(View.GONE);
        }
    }
}
