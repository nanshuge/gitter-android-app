package im.gitter.gitter.models;

import org.json.JSONException;
import org.json.JSONObject;

import im.gitter.gitter.content.ModelFactory;

public class Message {

    private final ModelFactory modelFactory = new ModelFactory();

    private final User fromUser;
    private final String text;
    private final String html;

    /*
    {
        id: '56f3df7f6d20f28b42f23805',
        text: ':+1: ',
        html: ':+1: ',
        sent: '2016-03-24T12:37:19.634Z',
        fromUser: {
            id: '5298e319ed5ab0b3bf04c984',
            username: 'mydigitalself',
            displayName: 'Mike Bartlett',
            url: '/mydigitalself',
            avatarUrlSmall: 'https://avatars1.githubusercontent.com/u/1491887?v=3&s=60',
            avatarUrlMedium: 'https://avatars1.githubusercontent.com/u/1491887?v=3&s=128',
            staff: true,
            v: 22,
            gv: '3'
        },
        unread: false,
        readBy: 26,
        urls: [],
        mentions: [],
        issues: [],
        meta: [],
        v: 1
    }
     */
    public Message(JSONObject json) throws JSONException {
        this.fromUser = modelFactory.createUser(json.getJSONObject("fromUser"));
        this.text = json.getString("text");
        this.html = json.getString("html");
    }

    public User getFromUser() {
        return fromUser;
    }

    public String getText() {
        return text;
    }

    public String getHtml() {
        return html;
    }
}
